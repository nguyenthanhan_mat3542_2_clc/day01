<?php
function openConnection()
{
    $host = "localhost";
    $user = "root";
    $pw = "";
    $name = "ltweb";
    $conn = new mysqli($host, $user, $pw, $name) or die("Connect failed: %s\n" . $conn->error);
    return $conn;
}
function closeConnection($conn)
{
    $conn->close();
}
