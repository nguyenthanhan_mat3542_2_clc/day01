<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <style>
        .container {
            width: 60%;
            margin: 2% auto 0 auto;
        }

        .search-box {
            display: flex;
            flex-direction: column;
        }

        label {
            display: inline-block;
            width: 10%;
            padding: 5px;
        }

        select,
        input[type="text"] {
            width: 20%;
            padding: 5px;
            margin-top: 5px;
        }

        button {
            font-family: inherit;
            font-size: 1rem;
            color: white;
            width: 10%;
            padding: 10px;
            background-color: #4f81bd;
            border-radius: 6px;
            margin: 1% auto 1% auto;
            cursor: pointer;
        }

        .btn {
            text-align: center;
        }

        .add-button {
            text-decoration: none;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid black;
        }

        th,
        td {
            padding: 10px;
            text-align: left;
        }

        .action {
            border: 1px solid #6889b2;
            background-color: #92b1d6;
            padding: 3px;
            color: white;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="search-box">
            <form method="post">
                <div class="department">
                    <label for="department">Khoa</label>
                    <select name="depts" id="getDept">
                        <option value="none" selected disabled hidden>--Chọn phân khoa--</option>
                        <option value="KHMT">Khoa học máy tính</option>
                        <option value="KHVL">Khoa học vật liệu</option>
                    </select>
                </div>
                <div class="search">
                    <label for="search">Từ khoá</label>
                    <input type="text" id="getName">
                </div>
                <div class="btn">
                    <button id="btn" type="submit" value="submit" name="search">Reset</button>
                </div>
            </form>
        </div>
        <p id="no">
        </p>
        <div class="add-btn">
            <a href="../day05/register.php" class="add-button"><button>Thêm</button></a>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="table"></tbody>
        </table>
    </div>
    <script>
        $(document).ready(function() {
            $('#getName').on("keyup", function() {
                var getName = $(this).val();
                var getDept = $('#getDept option:selected').text();
                $.ajax({
                    method: 'POST',
                    url: 'ajax.php',
                    data: {
                        name: getName,
                        dept: getDept
                    },
                    success: function(response) {
                        $("#table").html(response);
                        var numOfRecords = $('#table tr:visible').length;
                        $('#no').html('Số sinh viên tìm thấy: ' + numOfRecords);
                    }
                });
            });
        });
    </script>
</body>

</html>