<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký sinh viên</title>
    <style>
        .container {
            width: 60%;
            margin: 1% auto 0 auto;
        }

        .date-of-birth,
        .address {
            display: flex;
        }

        .label {
            font-weight: 600;
            margin-right: 10%;
            width: 10%;
        }
    </style>
</head>

<body>
    <div class="container">
        <h2 style="text-align: center;">Form đăng ký sinh viên</h2>
        <p id="fname-validation"></p>
        <p id="sex-validation"></p>
        <p id="dob-validation"></p>
        <form action="regist_student.php" method="post">
            <div class="full-name">
                <label for="full-name" class="label">Họ và tên</label>
                <input type="text" name="full-name" size="32" id="full-name" required>
            </div>
            <div class="sex">
                <label for="sex" class="label">Giới tính</label>
                <?php
                $sex = array(
                    "1" => "Nam",
                    "2" => "Nữ"
                );

                foreach ($sex as $value => $label) {
                    echo '<input type="radio" name="sex" value="' . $value . '" class="radio">' . $label;
                }
                ?>
            </div>
            <div class="date-of-birth">
                <label for="date-of-birth" class="label">Ngày sinh</label>
                <div class="year">
                    <label for="year">Năm</label>
                    <select name="years" id="">
                        <option value=""></option>
                        <?php
                        $currentYear = date("Y");
                        for ($year = $currentYear - 40; $year <= $currentYear - 15; $year++) {
                            echo "<option value=\"$year\">$year</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="month">
                    <label for="month">Tháng</label>
                    <select name="months">
                        <option value=""></option>
                        <?php
                        for ($month = 1; $month <= 12; $month++) {
                            echo "<option value=\"$month\">$month</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="day">
                    <label for="day">Ngày</label>
                    <select name="days">
                        <option value=""></option>
                        <?php
                        for ($day = 1; $day <= 31; $day++) {
                            echo "<option value=\"$day\">$day</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="address">
                <label for="address" class="label">Địa chỉ</label>
                <div class="city">
                    <label for="city">Thành phố</label>
                    <select id="city" name="city">
                        <option value=""></option>
                        <option value="hn">Hà Nội</option>
                        <option value="hcm">Hồ Chí Minh</option>
                    </select>
                </div>
                <div class="district">
                    <label for="district">Quận</label>
                    <select id="district" name="district">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="other">
                <label for="other" class="label">Thông tin khác</label>
                <textarea name="other" cols=30 rows=5></textarea>
            </div>
            <div class="btn">
                <button id="btn" type="submit" value="submit" name="submit">Đăng ký</button>
            </div>
        </form>
    </div>

    <script>
        // city -> distric
        var cityDropdown = document.getElementById('city');
        var districtDropdown = document.getElementById('district');

        var cityDistricts = {
            "hn": ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
            "hcm": ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 8"]
        };

        cityDropdown.addEventListener('change', function() {
            var selectedCity = cityDropdown.value;
            var districts = cityDistricts[selectedCity] || [];
            updateDistrictOptions(districts);
        });

        function clearDistrictOptions() {
            while (districtDropdown.options.length > 1) {
                districtDropdown.remove(1);
            }
        }

        function updateDistrictOptions(districts) {
            districts.forEach(function(district) {
                var option = document.createElement('option');
                option.value = district;
                option.text = district;
                districtDropdown.appendChild(option);
            });
        }

        //====================== VALIDATION SECTION =========================
        // full name
        var fname = document.getElementById("full-name");
        var p1 = document.getElementById("fname-validation");

        // sex
        var sex = document.getElementsByName("sex");
        var p2 = document.getElementById("sex-validation");

        // date of birth
        var year = document.getElementsByName("years")[0];
        var month = document.getElementsByName("months")[0];
        var day = document.getElementsByName("days")[0];
        var p3 = document.getElementById("dob-validation");

        var button = document.getElementById("btn");

        button.addEventListener("click", function() {
            //===========================
            if (fname.value == "") {
                p1.textContent = "Hãy nhập họ tên";
            } else {
                p1.textContent = "";
            }

            //===========================
            var checked = false;
            for (var i = 0; i < sex.length; i++) {
                if (sex[i].checked) {
                    checked = true;
                    break;
                }
            }
            if (!checked) {
                p2.textContent = "Hãy chọn giới tính";
            } else {
                p2.textContent = "";
            }

            //===========================
            if (year.value == "" || month.value == "" || day.value == "") {
                p3.textContent = "Hãy chọn ngày sinh";
            } else {
                p3.textContent = "";
            }

        });
    </script>
</body>

</html>