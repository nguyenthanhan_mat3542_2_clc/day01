<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận đăng ký</title>
    <style>
        .container {
            width: 70%;
            margin: 1% auto 0 auto;
        }

        .label {
            font-weight: 600;
            margin-right: 10%;
        }

        .full-name,
        .sex,
        .date-of-birth,
        .address,
        .other {
            display: flex;

        }
    </style>
</head>

<body>
    <?php
    $full_name = "";
    $sex = null;
    $day = null;
    $month = null;
    $year = null;
    $city = null;
    $district = null;
    $other = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["submit"])) {
        $full_name = $_POST["full-name"];
        $sex = isset($_POST["sex"]) ? $_POST["sex"] : "";
        $day = $_POST["days"];
        $month = $_POST["months"];
        $year = $_POST["years"];
        $city = $_POST["city"];
        $district = $_POST["district"];
        $other = $_POST["other"];
    }
    ?>
    <div class="container">
        <div class="full-name">
            <p class="label">Họ và tên</p>
            <?php
            echo '<p>' . $full_name . '</p>';
            ?>
        </div>
        <div class="sex">
            <p class="label">Giới tính</p>
            <?php
            echo "<p>" . ($sex == "1" ? "Nam" : "Nữ") . "</p>";
            ?>
        </div>
        <div class="date-of-birth">
            <p class="label">Ngày sinh</p>
            <?php
            if (!empty($day) && !empty($month) && !empty($year)) {
                $dateFormatted = $day . '/' . $month . '/' . $year;
                echo '<p>' . $dateFormatted . '</p>';
            }
            ?>
        </div>
        <div class="address">
            <p class="label">Địa chỉ</p>
            <?php
            echo "<p>" . $district . ' - ' . ($city == "hn" ? 'Hà Nội' : ($city == "hcm" ? 'Hồ Chí Minh' : '')) . "</p";
            ?>
        </div>
        <div class="other">
            <p class="label">Thông tin khác</p>
            <?php
            echo '<p>' . $other . '</p>';
            ?>
        </div>
    </div>

    <script>
        // city -> district
        var cityDropdown = document.getElementById('city');
        var districtDropdown = document.getElementById('district');

        var cityDistricts = {
            "hn": ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
            "hcm": ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 8"]
        };

        cityDropdown.addEventListener('change', function() {
            var selectedCity = cityDropdown.value;
            var districts = cityDistricts[selectedCity] || [];
            updateDistrictOptions(districts);
        });

        function clearDistrictOptions() {
            districtDropdown.innerHTML = '<option value=""></option>';
        }

        function updateDistrictOptions(districts) {
            districts.forEach(function(district) {
                var option = document.createElement('option');
                option.value = district;
                option.text = district;
                districtDropdown.appendChild(option);
            });
        }

        // validation
        var input = document.getElementById("full-name");

        // Get the paragraph element by its id
        var p = document.getElementById("fname-validation");

        // Add an event listener to the input element
        input.addEventListener("input", function() {
            // Check if the input value is empty or not
            if (input.value == "") {
                // If empty, show the message in the paragraph element
                p.textContent = "Hãy nhập họ tên";
            } else {
                // If not empty, clear the message in the paragraph element
                p.textContent = "";
            }
        });
    </script>
</body>

</html>