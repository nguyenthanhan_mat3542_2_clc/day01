<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <style>
        .container {
            width: 60%;
            margin: 2% auto 0 auto;
        }

        .search-box {
            display: flex;
            flex-direction: column;
        }

        label {
            display: inline-block;
            width: 10%;
            padding: 5px;
        }

        select,
        input[type="text"] {
            width: 20%;
            padding: 5px;
            margin-top: 5px;
        }

        .button {
            font-family: inherit;
            font-size: 1rem;
            color: white;
            width: 10%;
            padding: 10px;
            background-color: #4f81bd;
            border-radius: 6px;
            margin: 1% auto 1% auto;
            cursor: pointer;
        }

        .btn {
            text-align: center;
        }

        .add-button {
            text-decoration: none;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid black;
        }

        th,
        td {
            padding: 10px;
            text-align: left;
        }

        .action {
            border: 1px solid #6889b2;
            background-color: #92b1d6;
            padding: 3px;
            color: white;
        }

        #popup {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 300px;
            height: 150px;
            background-color: white;
            border: 1px solid black;
            padding: 10px;
            text-align: center;
            z-index: 9999;
        }

        .hidden {
            display: none;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="search-box">
            <form method="post">
                <div class="department">
                    <label for="department">Khoa</label>
                    <select name="depts" id="getDept">
                        <option value="none" selected disabled hidden>--Chọn phân khoa--</option>
                        <option value="KHMT">Khoa học máy tính</option>
                        <option value="KHVL">Khoa học vật liệu</option>
                    </select>
                </div>
                <div class="search">
                    <label for="search">Từ khoá</label>
                    <input type="text" id="getName">
                </div>
                <div class="btn">
                    <button class="button" type="submit" value="submit" name="search">Reset</button>
                </div>
            </form>
        </div>
        <p id="no">
        </p>
        <div class="add-btn">
            <a href="../day05/register.php" class="add-button"><button class="button">Thêm</button></a>
        </div>
        <div id="popup" class="hidden">
            <p>Bạn muốn xóa sinh viên này?</p>
            <button id="ok" onclick="deleteRow()">Xoá</button>
            <button id="cancel" onclick="closePopup()">Huỷ</button>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="table"></tbody>
        </table>
    </div>
    <script>
        $(document).ready(function() {
            $('#getName').on("keyup", function() {
                var getName = $(this).val();
                var getDept = $('#getDept option:selected').text();
                $.ajax({
                    method: 'POST',
                    url: 'ajax.php',
                    data: {
                        name: getName,
                        dept: getDept
                    },
                    success: function(response) {
                        $("#table").html(response);
                        var numOfRecords = $('#table tr:visible').length;
                        $('#no').html('Số sinh viên tìm thấy: ' + numOfRecords);
                    }
                });
            });
        });

        function showPopup(button) {
            var popup = document.getElementById("popup");
            popup.classList.remove("hidden");
            document.getElementById("ok").onclick = function() {

                var row = button.closest('tr');
                var studentId = row.getAttribute('data-student-id');

                deleteRow(studentId);
                closePopup();
            };
        }

        function closePopup() {
            var popup = document.getElementById("popup");

            popup.classList.add("hidden");
        }

        function deleteRow(studentId) {
            $.ajax({
                method: 'POST',
                url: 'delete.php',
                data: {
                    id: studentId
                },
                success: function(response) {
                    if (response === 'success') {
                        $('tr[data-student-id="' + studentId + '"]').remove();
                    } else {
                        alert('Failed to delete the record.');
                    }
                }
            });
        }
    </script>
</body>

</html>