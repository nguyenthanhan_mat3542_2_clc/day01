<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng nhập</title>
    <style>
        body {
            font-family: 'Times New Roman', Times, serif;
        }

        .outer-box {
            padding: 50px;
            width: 40%;
            border: 2px solid #648bae;
            margin: 10% auto 0 auto;
        }

        .datetime {
            width: 77%;
            margin-left: auto;
            margin-right: auto;
            background-color: #f2f2f2;
            padding: 10px;
        }

        .login-form {
            width: 80%;
            margin-left: auto;
            margin-right: auto;
        }

        .login-form {
            margin-top: 20px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .left {
            flex: 1;
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
            margin-right: 15px;
        }

        label {
            margin-bottom: 10px;
            padding: 10px;
            box-sizing: border-box;
            color: white;
            background-color: #5b9bd5;
            border: 2px solid #648bae;
        }

        .right {
            flex: 1;
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
        }

        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 11px;
            border: 1px solid #ccc;
            margin-bottom: 10px;
            box-sizing: border-box;
            border: 2px solid #648bae;
        }

        button {
            margin: 5% auto 0 auto;
            display: block;
            font-family: inherit;
            width: 25%;
            padding: 15px;
            border: 2px solid #648bae;
            background-color: #5b9bd5;
            color: white;
            border-radius: 10px;
            font-size: 1rem;
        }
    </style>
</head>

<body>
    <div class="outer-box">
        <div class="datetime">
            <?php
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $day_of_week = date("N") + 1;

            if ($day_of_week == "7") {
                $day_of_week == "Chủ nhật";
            }
            echo "Bây giờ là: " . date("H:i") . ", thứ " . $day_of_week . " ngày " . date("d/m/Y");
            ?>
        </div>
        <form>
            <div class="login-form">
                <div class="left">
                    <label for="fname">Tên đăng nhập</label>
                    <label for="lname">Mật khẩu</label>
                </div>
                <div class="right">
                    <input type="text" id="fname" name="username">
                    <input type="password" id="lname" name="password">
                </div>
            </div>
            <button>Đăng nhập</button>
        </form>
    </div>
</body>

</html>