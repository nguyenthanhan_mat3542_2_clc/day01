<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tân sinh viên</title>
    <style>
        body {
            font-family: 'Times New Roman', Times, serif;
        }

        .outer-box {
            width: 30%;
            margin: 10% auto 0 auto;
            padding: 3rem;
        }

        .outer-box,
        .label,
        input[type=text],
        select,
        button {
            border: 2px solid #658baf;
        }

        .label {
            display: inline-block;
            width: 20%;
            padding: 7px;
            margin-bottom: 4%;
            margin-right: 20px;
            background-color:
                #5b9bd5;
            color: white;
            text-align: center;
        }

        .full-name,
        .department {
            padding: 7px 0 7px 0;
        }

        .full-name {
            /* width: 50%; */
        }

        .radio {
            /* accent-color: #5b9bd5; */
            border: 2px solid #658baf;
        }

        button {
            font-family: inherit;
            font-size: 1rem;
            color: white;
            width: 30%;
            padding: 10px;
            background-color: #70ad47;
            border-radius: 6px;
            margin: 1% auto 0 auto;
            cursor: pointer;
        }

        .btn {
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="outer-box">
        <form action="POST">
            <div class="full-name">
                <label for="full-name" class="label">Họ tên</label>
                <input type="text" name="full-name" size="32" class="full-name">
            </div>
            <div class="sex">
                <label for="sex" class="label">Giới tính</label>
                <?php
                $sex = array(
                    "Nam", "Nữ"
                );

                for ($i = 0; $i < count($sex); $i++) {
                    $radio = $sex[$i];
                ?>
                    <input name="sex" type="radio" class="radio" value="<?= $radio ?>"><?= $radio ?>
                    <!-- <label value=""><?= $radio ?></label> -->
                <?php } ?>

            </div>
            <div class="department">
                <label for="department" class="label">Phân khoa</label>
                <select name="department" class="department">
                    <option value="none" selected disabled hidden>--Chọn phân khoa--</option>
                    <?php
                    $departments = array(
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học vật liệu"
                    );
                    foreach ($departments as $d => $name) { ?>
                        <option value="<?= $d ?>"><?= $name ?></option>

                    <?php } ?>
                </select>
            </div>
            <div class="btn">
                <button>Đăng ký</button>
            </div>
        </form>
    </div>

</body>

</html>